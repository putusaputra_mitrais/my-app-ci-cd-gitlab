node{

    // declare variables
    //def commitHash = ""
    def commitSubject = ""
    def committerName = ""
    def committerEmail = ""
    def buildStatus = "failed"

    try {
        // notify gitlab
        updateGitlabCommitStatus name: 'build', state: 'running'

        // this will run only if successful
        stage('SCM Checkout'){
            git branch:'master',
            url:'https://gitlab.com/putusaputra_mitrais/my-app-ci-cd-gitlab.git'

            // get git variables
            //commitHash = sh(returnStdout: true, script: 'git log -1 --pretty="format:%H"')
            commitSubject = sh(returnStdout: true, script: 'git log -1 --pretty="format:%s"')
            committerName = sh(returnStdout: true, script: 'git log -1 --pretty="format:%cn"')
            committerEmail = sh(returnStdout: true, script: 'git log -1 --pretty="format:%ce"')
            currentBranchName = sh(returnStdout: true, script: 'git rev-parse --abbrev-ref HEAD')
        }
        stage('Compile-Package'){
            sh 'mvn clean package'
        }
        stage('SonarQube Analysis'){
            withSonarQubeEnv('sonar-6'){
                sh "mvn sonar:sonar"
            }
        }
        stage("Quality Gate Status Check"){
            timeout(time: 1, unit: 'HOURS') {
                def qg = waitForQualityGate()
                if (qg.status != 'OK') {
                    // mark this build as error and abort next pipeline stage
                    error "Pipeline aborted due to quality gate failure: ${qg.status}"
                }
            }
        }
        stage("Deploy jar file"){
            sh "cp target/gs-maven-0.1.0.jar D:/Putra/CDC/team-7/test-deploy-jenkins-gitlab/${currentBranchName}"
        }  

        currentBuild.result = 'SUCCESS'
    } catch (e) {
        // this will run only if failed
        currentBuild.result = 'FAILURE'
        throw e
    } finally {
        // this will always run
        if (currentBuild.result == 'SUCCESS') {
            buildStatus = "successful"
            updateGitlabCommitStatus name: 'build', state: 'success'
        } else {
            buildStatus = "failed"
            updateGitlabCommitStatus name: 'build', state: 'failed'
        }

        // send email to notify committer if the build failed or success
        mail bcc: '', 
        body: "Hi ${committerName} !,\n\nyour build for commit \"${commitSubject}\" is ${buildStatus}.\nGo to this link for more detail : ${env.BUILD_URL}", 
        cc: '', 
        from: '', 
        replyTo: '', 
        subject: "Jenkins Job ${env.JOB_NAME} with commit ${commitSubject} is ${buildStatus}", 
        to: "${committerEmail}"
    }
}